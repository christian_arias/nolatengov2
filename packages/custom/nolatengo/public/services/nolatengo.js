(function() {
    'use strict';

    function Nolatengo($http, $q) {
        return {
            name: 'nolatengo',
            checkCircle: function(circle) {
                var deferred = $q.defer();

                $http.get('/api/nolatengo/example/' + circle).success(function(response) {
                    deferred.resolve(response);
                }).error(function(response) {
                    deferred.reject(response);
                });
                return deferred.promise;

            }
        };
    }

    angular
        .module('mean.nolatengo')
        .factory('Nolatengo', Nolatengo);

    Nolatengo.$inject = ['$http', '$q'];

})();
