(function() {
    'use strict';

    /* jshint -W098 */

    function NolatengoController($scope, Global, Nolatengo, $stateParams) {
        $scope.global = Global;
        $scope.package = {
            name: 'nolatengo'
        };

        $scope.checkCircle = function() {
            Nolatengo.checkCircle($stateParams.circle).then(function(response) {
                $scope.res = response;
                $scope.resStatus = 'info';
            }, function(error) {
                $scope.res = error;
                $scope.resStatus = 'danger';
            });
        };
    }

    angular
        .module('mean.nolatengo')
        .controller('NolatengoController', NolatengoController);

    NolatengoController.$inject = ['$scope', 'Global', 'Nolatengo', '$stateParams'];

})();
