(function() {
    'use strict';

    function Nolatengo($stateProvider) {
        $stateProvider.state('nolatengo example page', {
            url: '/nolatengo/example',
            templateUrl: 'nolatengo/views/index.html'
        }).state('nolatengo circles example', {
            url: '/nolatengo/example/:circle',
            templateUrl: 'nolatengo/views/example.html'
        });
    }

    angular
        .module('mean.nolatengo')
        .config(Nolatengo);

    Nolatengo.$inject = ['$stateProvider'];

})();
